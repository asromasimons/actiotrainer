<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
	<title>Material Kit by Creative Tim</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">

	<!-- CSS Files -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/');?>slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/frontend/');?>slick/slick-theme.css">
	<link href="<?php echo base_url('assets/frontend/');?>css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url('assets/frontend/');?>css/material-kit.css" rel="stylesheet"/>
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="<?php echo base_url('assets/frontend/');?>css/demo.css" rel="stylesheet" />
	<link href="<?php echo base_url('assets/frontend/');?>css/custom.css" rel="stylesheet"/>

	<style type="text/css">


		.slider {
			width: 80%;
			margin: 0px auto;
			background-color:#ffffff;
		}

		.slick-slide {
			margin: 0px 20px;
		}

		.slick-slide img {
			width: 100%;
		}

		.slick-track{
			padding:5px;
		}

		.slick-prev:before,
		.slick-next:before {
			color: black;
		}
	</style>

</head>

<body class="index-page">
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="http://www.creative-tim.com">
				<div class="logo-container">
					<div class="logo">
						<img src="<?php echo base_url('assets/frontend/');?>img//home/logo.png" >
					</div>



				</div>
			</a>
		</div>

		<div class="collapse navbar-collapse" id="navigation-index">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="components-documentation.html" target="_blank">
						HOME
					</a>
				</li>
				<li>
					<a href="components-documentation.html" target="_blank">
						COME FUNZIONA
					</a>
				</li>
				<li>
					<a href="components-documentation.html" target="_blank">
						BLOG
					</a>
				</li>
				<li>
					<a href="components-documentation.html" target="_blank" class="buttonlogin">
						LOGIN
					</a>
				</li>
				<li>
					<a href="components-documentation.html" target="_blank" class="buttonregister">
						REGISTRATI
					</a>
				</li>



			</ul>
		</div>
	</div>
</nav>
<!-- End Navbar -->

<div class="wrapper">
	<div class="header" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/home/homeheader.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="brand">
						<h1 class="textheaderbold">MOVE YOUR BODY</h1>
						<h1 class="textheaderlight nodesk">BUILD YOUR MIND</h1>
					</div>
				</div>
				<div class="col-md-6">
					<div class="brand">
						<h1 class="textheaderlight nomobile">BUILD YOUR MIND</h1>
					</div>
				</div>
			</div>

		</div>
	</div>



	<div class="container">

		<div class="boxricerca">

			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons"><img src="<?php echo base_url('assets/frontend/');?>img/home/iconsport.png" width="40"></i>
					</span>
					<input type="text" id="disciplina" class="form-control campiform" placeholder="cerca per disciplina sportiva">
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons"><img src="<?php echo base_url('assets/frontend/');?>img/home/iconlocation.png" width="40"></i>
					</span>
					<input type="text" id="location" class="form-control campiform" placeholder="cerca per indirizzo,CAP,città">
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit" class="buttonricerca"><img src="<?php echo base_url('assets/frontend/');?>img/home/iconsearch.png" width="20">Effettua Ricerca</button>
				<div class="sfondoriga">
				<button type="submit" class="buttonricercaavanzata"><img src="<?php echo base_url('assets/frontend/');?>img/home/iconavanzata.png" width="20">Ricerca avanzata</button>
				</div>
			</div>

		</div>

	</div>

	<section id="ricercacorsi" style="margin-bottom:80px;">
		<div class="container cerca">
			<h3 style="font-family:'Ubuntu';color:#9d1c7e;">Ricerca tra i corsi</h3>
			<hr />


		</div>

		<section class="responsive slider">
			<div class="boxcorsi">
				<div class="imgsfondocorsi" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/avatar.jpg');">
					<div class="gradient"></div>
				</div>
				<div class="textcorsi">
					<h6 style="text-transform:uppercase;margin-bottom:5px;color:#ed3b4f;">Mario Rossi</h6>
					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:5px;">yoga</h3>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Villa Torlonia</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Giovedi alle 19</h6>
					<button type="submit" class="buttondettaglicorso">Vedi Dettagli</button>
				</div>
			</div>
			<div class="boxcorsi">
				<div class="imgsfondocorsi" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/2.jpg');">
					<div class="gradient"></div>
				</div>
				<div class="textcorsi">
					<h6 style="text-transform:uppercase;margin-bottom:5px;color:#ed3b4f;">Mario Rossi</h6>
					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:5px;">yoga</h3>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Villa Torlonia</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Giovedi alle 19</h6>
					<button type="submit" class="buttondettaglicorso">Vedi Dettagli</button>
				</div>
			</div>
			<div class="boxcorsi">
				<div class="imgsfondocorsi" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/3.jpg');">
					<div class="gradient"></div>
				</div>
				<div class="textcorsi">
					<h6 style="text-transform:uppercase;margin-bottom:5px;color:#ed3b4f;">Mario Rossi</h6>
					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:5px;">yoga</h3>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Villa Torlonia</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Giovedi alle 19</h6>
					<button type="submit" class="buttondettaglicorso">Vedi Dettagli</button>
				</div>
			</div>
			<div class="boxcorsi">
				<div class="imgsfondocorsi" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/4.jpg');">
					<div class="gradient"></div>
				</div>
				<div class="textcorsi">
					<h6 style="text-transform:uppercase;margin-bottom:5px;color:#ed3b4f;">Mario Rossi</h6>
					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:5px;">yoga</h3>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Villa Torlonia</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Giovedi alle 19</h6>
					<button type="submit" class="buttondettaglicorso">Vedi Dettagli</button>
				</div>
			</div>
			<div class="boxcorsi">
				<div class="imgsfondocorsi" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/5.jpg');">
					<div class="gradient"></div>
				</div>
				<div class="textcorsi">
					<h6 style="text-transform:uppercase;margin-bottom:5px;color:#ed3b4f;">Mario Rossi</h6>
					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:5px;">yoga</h3>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Villa Torlonia</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Giovedi alle 19</h6>
					<button type="submit" class="buttondettaglicorso">Vedi Dettagli</button>
				</div>
			</div>

		</section>

		<div class="container" style="text-align:center;margin-top:60px;">
			<h3 style="font-family:'Ubuntu';color:#9d1c7e;">Ricerca tra i trainer</h3>
			<hr />


		</div>

		<section class="responsive slider">
			<div class="boxtrainer">
				<div class="imgsfondotrainer" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/avatar.jpg');">
					<div class="gradienttrainer">
					</div>
				</div>
				<div class="textcorsi" style="text-align:center;">

					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:10px;">mario rossi</h3>
					<div class="raiting" style="margin-bottom:10px;">
					<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;">
					</div>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Tennis,Yoga,Pugilato</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Luogo di operatività: Roma</h6>

				</div>
			</div>
			<div class="boxtrainer">
				<div class="imgsfondotrainer" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/2.jpg');">
					<div class="gradienttrainer">
					</div>
				</div>
				<div class="textcorsi" style="text-align:center;">

					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:10px;">mario rossi</h3>
					<div class="raiting" style="margin-bottom:10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;">
					</div>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Tennis,Yoga,Pugilato</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Luogo di operatività: Roma</h6>

				</div>
			</div>
			<div class="boxtrainer">
				<div class="imgsfondotrainer" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/3.jpg');">
					<div class="gradienttrainer">
					</div>
				</div>
				<div class="textcorsi" style="text-align:center;">

					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:10px;">mario rossi</h3>
					<div class="raiting" style="margin-bottom:10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;">
					</div>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Tennis,Yoga,Pugilato</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Luogo di operatività: Roma</h6>

				</div>
			</div>
			<div class="boxtrainer">
				<div class="imgsfondotrainer" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/4.jpg');">
					<div class="gradienttrainer">
					</div>
				</div>
				<div class="textcorsi" style="text-align:center;">

					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:10px;">mario rossi</h3>
					<div class="raiting" style="margin-bottom:10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;">
					</div>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Tennis,Yoga,Pugilato</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Luogo di operatività: Roma</h6>

				</div>
			</div>
			<div class="boxtrainer">
				<div class="imgsfondotrainer" style="background-image: url('<?php echo base_url('assets/frontend/');?>img/5.jpg');">
					<div class="gradienttrainer">
					</div>
				</div>
				<div class="textcorsi" style="text-align:center;">

					<h3 style="text-transform:uppercase;margin-top:0px;color:#ed3b4f;margin-bottom:10px;">mario rossi</h3>
					<div class="raiting" style="margin-bottom:10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votosi.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;margin-right: -10px;">
						<img src="<?php echo base_url('assets/frontend/');?>img/home/votono.png" style="width:50px;display:inline-block;">
					</div>
					<h4 style="margin-top:0px;font-family:'Ubuntu';color:#9d1c7e;margin-bottom:5px;">Tennis,Yoga,Pugilato</h4>
					<h6 style="margin-top:0px;font-family:'Ubuntu';text-transform:none;color:#777777;margin-bottom:5px;">Luogo di operatività: Roma</h6>

				</div>
			</div>

		</section>

	</section>





	<footer class="footer" style="padding-top:30px;padding-bottom:20px;">
		<div class="container" style="width:95%;">
			<div class="col-md-4">
				<div class="col-md-4"><p>FAQ<br />CONTATTI</p></div>
				<div class="col-md-4"><p>TERMINI E CONDIZIONI<br />PRIVACY</p></div>
			</div>
			<div class="col-md-4" style="text-align:center;font-family:'Ubuntu';color:#ed3b4f;">
				ACTIOTRAINER s.r.l
<br />
				Via Lorem Ipsum n.37, 01055 Roma
<br />
				P.IVA 02848390122
			</div>
			<div class="col-md-4" style="text-align:right;">
				<img src="<?php echo base_url('assets/frontend/');?>img/home/fb.png" style="width:30px;display:inline-block;margin-right: 10px;">
				<img src="<?php echo base_url('assets/frontend/');?>img/home/tw.png" style="width:30px;display:inline-block;margin-right: 10px;">
				<img src="<?php echo base_url('assets/frontend/');?>img/home/g.png" style="width:30px;display:inline-block;margin-right: 10px;">
				<img src="<?php echo base_url('assets/frontend/');?>img/home/ins.png" style="width:30px;display:inline-block;">
				<br />
				<img src="<?php echo base_url('assets/frontend/');?>img/home/cloud.png" style="width:50px;margin-top:20px;">

			</div>
		</div>
	</footer>
</div>



</body>
<!--   Core JS Files   -->


<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/frontend/');?>slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$('.responsive').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [

			{
				breakpoint: 1300,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
</script>


<script src="<?php echo base_url('assets/frontend/');?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/frontend/');?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/frontend/');?>js/material.min.js"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?php echo base_url('assets/frontend/');?>js/nouislider.min.js" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="<?php echo base_url('assets/frontend/');?>js/bootstrap-datepicker.js" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="<?php echo base_url('assets/frontend/');?>js/material-kit.js" type="text/javascript"></script>

<script type="text/javascript">

	$().ready(function(){
		// the body of this function is in assets/material-kit.js
		materialKit.initSliders();
		window_width = $(window).width();

		if (window_width >= 992){
			big_image = $('.wrapper > .header');

			$(window).on('scroll', materialKitDemo.checkScrollForParallax);
		}

	});
</script>

</html>
